import * as backoff from 'backoff';
import * as log from 'fancy-log';
import * as cassandrom from 'cassandrom';

export interface ConnectOptions {

    name?: string;
    host?: string;
    port?: string;
    tries?: number;
    initialDelay?: number;
    maxDelay?: number;

}

function loadPkjJson(attempts: number = 1) {

    if (attempts > 5) {

        return 'default';

    }

    const mainPath = attempts === 1 ? './' : Array(attempts).join('../');

    try {

        return require.main ? require.main.require(mainPath + 'package.json') : '';

    } catch (e) {

        return loadPkjJson(attempts + 1);

    }
}

function connect(options: ConnectOptions): Promise<void> {

    return new Promise((resolve, reject) => {

        const {

            name = loadPkjJson(),
            host = 'database',
            tries = 32,
            initialDelay = 500,
            maxDelay = 4096000,

        } = options;

        const expBackoff = backoff.exponential({ initialDelay, maxDelay });
        expBackoff.failAfter(tries);
        expBackoff.on('backoff', (num, delay) => {

            if (num > 0) {

                log.error('Could not connect to CassandraDB. Retrying in ' + (delay / 1000) + 's');

            }
        });

        expBackoff.on('ready', () => {

            (cassandrom as any).Promise = global.Promise;
            const conn = cassandrom.createConnection({ contactPoints: [ host ], keyspace: name, createKeyspace: true, createTables: true }, () => {});
            conn.on('error', (err) => {

                log.error(err);
                expBackoff.backoff();

            });

            if (cassandrom.connection === null) {

                expBackoff.backoff();

            } else {

                expBackoff.reset();
                return resolve();

            }
        });

        expBackoff.on('fail', () => {

            log.error('Failed to connect to CassandraDB');
            return reject();

        });

        expBackoff.backoff();

    });
}

export default connect;
